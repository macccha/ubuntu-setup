# Install ROS
```bash
wget https://gitlab.com/macccha/ubuntu-setup/-/raw/main/ros_install.sh && chmod 755 ros_install.sh && ./ros_install.sh melodic
```
# Install 
Add to .bashrc
```bash
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash
ROS_WORKSPACE=${HOME}/catkin_ws
# Set ROS alias command
alias cw="cd ${ROS_WORKSPACE}"
alias cs="cd ${ROS_WORKSPACE}/src"
alias cm="cd ${ROS_WORKSPACE} && catkin build && source devel/setup.bash && cd -"
```
Download GitKraken
```bash
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
```
